//
//  FirstViewCotrollerMVVM.swift
//  Basic_MVVM_Swift_Navigation
//
//  Created by Sumit Vishwakarma on 17/05/23.
//

import Foundation

class FirstVCViewModel {
    var isisNavigate = false
    init(){}
    
    func isNavigationSuccess(closure:@escaping (Bool)->()) {
        self.isisNavigate = true
        closure(self.isisNavigate)
    }
     
}
