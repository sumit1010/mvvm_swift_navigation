//
//  ViewController.swift
//  Basic_MVVM_Swift_Navigation
//
//  Created by Sumit Vishwakarma on 17/05/23.
//

import UIKit

class FirstViewControler: UIViewController {
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    var firstVM = FirstVCViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
    }
    
    @IBAction func LoginButtonAction(_ sender: UIButton) {
        
        let validationTrue = self.loginValidations() == "" ? true : false
        if validationTrue {
            firstVM.isNavigationSuccess { [weak self] isSuccess in
                if isSuccess {
                    let secondVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SecondViewController") as! SecondViewController;
                    self?.navigationController?.pushViewController(secondVC, animated: true)
                }
            }
        } else {
            self.showAlert(message: self.loginValidations())
        }
    }
}

extension FirstViewControler {
    
    func loginValidations() -> String {
        if  emailTextField.text == "" {
            return "Enter username"
        }
        if passwordTextField.text == "" {
            return "Enter password"
        }
        return ""
    }
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: message, message: "", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(ok)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
}
